import requests
import json


def getplayerdata(player, stats=1, achievements=1, deaths=1, accountinfo=1, othercharacters=1):
    playere = player.replace(" ", "+")
    req = requests.get("https://api.tibiadata.com/v2/characters/" + playere + ".json")
    req_decoded = json.loads(req.text)
    if stats:
        getstats(req_decoded, player)
    if achievements:
        getachievements(req_decoded, player)
    if deaths:
        getdeaths(req_decoded, player)
    if accountinfo:
        getaccount_information(req_decoded, player)
    if othercharacters:
        getothercharacters(req_decoded, player)


def getachievements(req, player):
    print "-" * 40 + "The achievements of " + player + "-" * 40
    for i in req["characters"]["achievements"]:
        print i["name"] + " *" * i["stars"]


def getstats(req, player):
    print "-" * 40 + "The stats of " + player + "-" * 40
    for i in req["characters"]["data"]:
        z = req["characters"]["data"].get(i)
        if str(i) == "last_login":
            print str(i) + " : " + str(z[0]["date"])
            continue
        elif str(i) == "guild":
            print str(i) + " : " + str(z["name"])
            print "rank_on_guild" + " : " + str(z["rank"])
            continue
        elif str(i) == "house":
            print str(i) + " : The house is in the world " + str(z["world"]) + \
                  ", in the town of " + str(z["town"]) + \
                  ", the ID of the house is " + str(z["houseid"]) + \
                  ", the name is " + unicode(z["name"]) + \
                  " and it was paid on " + str(z["paid"])
            continue
        elif str(i) == "former_names":
            print str(i) + " : " + unicode(", ".join(z))
            continue
        print str(i) + " : " + unicode(z)


def getdeaths(req, player):
    print "-" * 40 + "The lasts deaths of " + player + "-" * 40
    a = req["characters"]["deaths"]
    for i in a:
        print "-" * 10 + i["reason"].replace("\r\n", " ") + "-" * 10
        for j in i:
            if j == "date":
                print j + " : " + i[j]["date"]
            elif j == "reason":
                continue
            elif j == "involved":
                print "involved : " + ", ".join(x["name"] for x in i["involved"])
            else:
                print unicode(j) + " : " + unicode(i[j])


def getaccount_information(req, player):
    print "-" * 40 + "Account information about " + player + "-" * 40
    a = req["characters"]["account_information"]
    for i in a:
        b = a.get(i)
        if i == "created":
            print i + " : " + unicode(b["date"])
        else:
            print i + " : " + unicode(b)


def getothercharacters(req, player):
    print "-" * 40 + "Other characters of " + player + "-" * 40
    a = req["characters"]["other_characters"]
    for i in a:
        for j in i:
            b = i.get(j)
            print j + " : " + unicode(b)
        print "-" * 20


def getguildsbyworld(world):
    req = requests.get("https://api.tibiadata.com/v2/guilds/" + world + ".json")
    req_decoded = json.loads(req.text)
    a = req_decoded["guilds"]["active"]
    print "-" * 40 + "Guilds of " + world + "-" * 40
    for i in a:
        for j in i:
            print j + " : " + unicode(i.get(j))


def getguildsnamebyworld(world):
    req = requests.get("https://api.tibiadata.com/v2/guilds/" + world + ".json")
    req_decoded = json.loads(req.text)
    a = req_decoded["guilds"]["active"]
    for i in a:
        print unicode(i.get("name"))


def getguilddata(guild, data=1, members=1, membersraw=1):
    guilde = guild.strip().replace(" ", "+")
    req = requests.get("https://api.tibiadata.com/v2/guild/" + guilde + ".json")
    req_decoded = json.loads(req.text)
    if data:
        getguildinformation(req_decoded, guild)
    if members:
        getguildmembers(req_decoded, guild)
    if membersraw:
        getguildmembersraw(req_decoded, guild)


def getguildinformation(req, guild):
    print "-" * 40 + "Information about " + guild + "-" * 40
    a = req["guild"]["data"]
    for i in a:
        if i == "guildhall":
            b = a[i]
            print str(i) + " : The house is in the world " + str(b["world"]) + \
                  ", in the town of " + str(b["town"]) + \
                  ", the ID of the house is " + str(b["houseid"]) + \
                  ", the name is " + unicode(b["name"]) + \
                  " and it was paid on " + str(b["paid"])
        else:
            print i + " : " + unicode(a[i])


def getguildmembers(req, guild):
    print "-" * 40 + "Members of " + guild + "-" * 40
    a = req["guild"]["members"]
    for i in a:
        print "-" * 40 + i["rank_title"] + " members" + "-" * 40
        for j in i["characters"]:
            for k in j:
                print k + " : " + unicode(j[k])
            print "-"*20


def getguildmembersraw(req, guild):
    print "-" * 40 + "Members (raw) of " + guild + "-" * 40
    a = req["guild"]["members"]
    for i in a:
        for j in i["characters"]:
            print j["name"]


if __name__ == '__main__':
    """ 
    
    If you want to get all data of a specific player:
    getplayerdata('username')
    
    If you want to discard any type of data:
    getplayerdata('username', stats=0, deaths=0)
    
    Type of data for players:
    stats
    achievements
    deaths
    accountinfo
    othercharacters
    
    If you want to get all guilds of a world:
    getguildsbyworld('world')
    
    If you want to get all names of the guilds of a world:
    getguildsnamebyworld('world')
    
    If you want to get all data of a specific guild:
    getguilddata('guild')
    
    If you want to discard any type of data
    getguilddata('guild', members=0)
    getguilddata('guild', data=0)
    getguilddata('guild', membersraw=0)
    
    """
    # getplayerdata("Pappa Rocky",1,1,0,1,0)
    # print "\n"*5
    # getplayerdata("Kalisiiros Miriobu",1,0,0,1,1)
    # print "\n" * 5
    # getplayerdata("Kaalisiros Miriobu",0,1,0,1,0)
    # print "\n" * 5
    # getplayerdata("Professorn Pierre",1,0,1,1,1)
    # print "\n" * 5
    # getplayerdata("Kitiara")
    # getguildsnamebyworld("Epoca")
    # getguilddata("Elysium")
