import TibiAPYthon as api
import sys


def menu():
    exitflag = 1
    while exitflag:
        print "*" * 80 + "\n TibiAPYthon 1.0 \n" + "*" * 80
        print "1. Get Player Data"
        print "2. Get Guilds by World"
        print "3. Get Guild Data"
        print "0. Exit \n" + "*" * 80 + "\n"
        r = input("Option: ")
        if r == 0:
            sys.exit()
        elif r == 1:
            print "*" * 80 + "\n"
            s = raw_input("What player: ")
            api.getplayerdata(s)
        elif r == 2:
            print "*" * 80 + "\n"
            s = raw_input("What world: ")
            api.getguildsbyworld(s)
        elif r == 3:
            print "*" * 80 + "\n"
            s = raw_input("What guild: ")
            api.getguilddata(s)
        print "*" * 80


if __name__ == '__main__':
    menu()
